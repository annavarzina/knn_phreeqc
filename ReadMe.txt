===============================================================================
Description:

    /01_ch folder contains scripts to generate data and to teach kNN model with 
n-dimensional input and output.
    
    In order to install it follow the instructions https://pypi.org/project/IPhreeqcPy/
or use .exe file in /setup folder in this directory.

    /src/ch/01_generate table.py is used to create a data set with IPhreeqc and save 
it in pickle (.pkl) format in /data folder. 1D data set has only 1 parameter with
changing values, while others are fixed. 2D data set has 2 sequential 
parameters (for example Ca and C concentrations).
    kNN scripts train the model based on saved data and the score is measured.
===============================================================================
Developer: Anna Varzina.

