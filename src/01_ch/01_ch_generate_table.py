# -*- coding: utf-8 -*-

import IPhreeqcPy
import numpy as np
import time
import pandas as pd
import sys, os
root_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(root_dir)

#%% FUNCTIONS
def phrqc_string_init(c, ca, ch, cc, water, n):
    '''
    Generate N cells with given initial conditions
    '''
    phrqc_input = []    
    for i in np.arange(1, n+1):
        phrqc_input.append('SOLUTION\t10000' + str(i))
        phrqc_input.append('\t-units\tmol/kgw')
        phrqc_input.append('\t-water\t'+ str(water[i-1]))
        phrqc_input.append('\tpH\t7\tcharge')
        phrqc_input.append('\tC\t' + str(c[i-1]) )
        phrqc_input.append('\tCa\t' + str(ca[i-1]) + '\n')
        phrqc_input.append('EQUILIBRIUM_PHASES\t10000'+ str(i))
        phrqc_input.append('\tcalcite\t0\t' + str(cc[i-1]) )
        phrqc_input.append('\tportlandite\t0\t' + str(ch[i-1]) + '\n')
    
   
    phrqc_input.append('SELECTED_OUTPUT')
    phrqc_input.append('\t-reset false')
    phrqc_input.append('\t-time false')
    phrqc_input.append('\t-high_precision true')
    phrqc_input.append('\t-solution true')
    phrqc_input.append('\t-pH true')
    phrqc_input.append('\t-pe true')
    phrqc_input.append('\t-charge_balance false')
    phrqc_input.append('\t-alkalinity true')
    phrqc_input.append('\t-ionic_strength false')
    phrqc_input.append('\t-percent_error false')
    
    phrqc_input.append('USER_PUNCH')
    phrqc_input.append('\t10\tpunch\ttot("C")')
    phrqc_input.append('\t20\tpunch\ttot("Ca")')
    phrqc_input.append('\t30\tpunch\ttot("O")')
    phrqc_input.append('\t40\tpunch\ttot("H")')
    phrqc_input.append('\t50\tpunch\tequi("portlandite")')
    phrqc_input.append('\t60\tpunch\tSI("portlandite")')
    phrqc_input.append('\t70\tpunch\tequi("calcite")')
    phrqc_input.append('\t80\tpunch\tSI("calcite")')
    phrqc_input.append('\t90\tpunch\ttot("water")')
    phrqc_input.append('\t100\tpunch\ttot("H2O")')
    phrqc_input.append('\t110\tpunch')
    phrqc_input.append('\t-headings\tC\tCa\tO\tH\tportlandite\tSI_portlandite' +
                       '\tcalcite\tSI_calcite\twater\tH2O')
    phrqc_input.append('\t-start')
    phrqc_input.append('\t-end')
    phrqc_input.append('end')
    return '\n'.join(phrqc_input)


def phrqc_string_react(n):
    phrqc_input = []   
    for i in np.arange(1, n+1):
        phrqc_input.append('copy cell 10000'+str(i) + ' ' + str(i))
    phrqc_input.append('end')
    phrqc_input.append('RUN_CELLS')
    phrqc_input.append('\t-cells 1-' + str(n+1))
    phrqc_input.append('\t-start_time 0')
    phrqc_input.append('\t-time_step 0')
    #phrqc_input.append('end')    
   
    phrqc_input.append('SELECTED_OUTPUT')
    phrqc_input.append('\t-reset false')
    phrqc_input.append('\t-time false')
    phrqc_input.append('\t-high_precision true')
    phrqc_input.append('\t-solution true')
    phrqc_input.append('\t-pH true')
    phrqc_input.append('\t-pe true')
    phrqc_input.append('\t-charge_balance false')
    phrqc_input.append('\t-alkalinity true')
    phrqc_input.append('\t-ionic_strength false')
    phrqc_input.append('\t-percent_error false')
    
    phrqc_input.append('USER_PUNCH')
    phrqc_input.append('\t10\tpunch\ttot("C")')
    phrqc_input.append('\t20\tpunch\ttot("Ca")')
    phrqc_input.append('\t30\tpunch\ttot("O")')
    phrqc_input.append('\t40\tpunch\ttot("H")')
    phrqc_input.append('\t50\tpunch\tequi("portlandite")')
    phrqc_input.append('\t60\tpunch\tSI("portlandite")')
    phrqc_input.append('\t70\tpunch\tequi("calcite")')
    phrqc_input.append('\t80\tpunch\tSI("calcite")')
    phrqc_input.append('\t90\tpunch\ttot("water")')
    phrqc_input.append('\t100\tpunch\ttot("H2O")')
    phrqc_input.append('\t110\tpunch')
    phrqc_input.append('\t-headings\tC\tCa\tO\tH\tportlandite\tSI_portlandite' +
                       '\tcalcite\tSI_calcite\twater\tH2O')
    phrqc_input.append('\t-start')
    phrqc_input.append('\t-end')
    phrqc_input.append('end')
    return '\n'.join(phrqc_input)

def phreeqc_generate_output(c, ca, ch, cc, water, n_cases):
    ps1 = phrqc_string_init(c, ca, ch, cc, water, n_cases)
    ps2 = phrqc_string_react(n_cases)
    IPhreeqc = IPhreeqcPy.IPhreeqc()
    IPhreeqc.LoadDatabase('C:\Anaconda2\lib\site-packages\databases\cemdata07.dat')
    
    it=time.time() 
    IPhreeqc.RunString(ps1)
    so1 = IPhreeqc.GetSelectedOutputArray()
    print('Initialization time %s'%str(time.time()-it))
    
    it=time.time() 
    IPhreeqc.RunString(ps2)
    so2 = IPhreeqc.GetSelectedOutputArray()
    print('Reaction time %s'%str(time.time()-it))
    # i = initialization/input, o= output
    res = {'i_sol':[row[0] for row in so1][1:-1], #solution number
           'i_pH': [row[1] for row in so1][1:-1], #pH
           'i_pe': [row[2] for row in so1][1:-1], #pe
           'i_Alk': [row[3] for row in so1][1:-1], #alkalinity
           'i_C': [row[4] for row in so1][1:-1], #carbon concentration
           'i_Ca': [row[5] for row in so1][1:-1], #Ca concentration
           'i_O': [row[6] for row in so1][1:-1], #O concentration
           'i_H': [row[7] for row in so1][1:-1], #H concentration
           'i_CH': ch,#[row[8] for row in so1][1:-1], #portlanite
           'i_SI_CH': [row[9] for row in so1][1:-1],  #sat.index for portlandite
           'i_CC': cc,#[row[10] for row in so1][1:-1], #calcite
           'i_SI_CC': [row[11] for row in so1][1:-1], #sat.index for calcite
           'i_poros': [row[12] for row in so1][1:-1], #water
           'i_H20': [row[13] for row in so1][1:-1],
           'o_sol':[row[0] for row in so2][1::],
           'o_pH': [row[1] for row in so2][1::],
           'o_pe': [row[2] for row in so2][1::],
           'o_Alk': [row[3] for row in so2][1::],
           'o_C': [row[4] for row in so2][1::],
           'o_Ca': [row[5] for row in so2][1::],
           'o_O': [row[6] for row in so2][1::],
           'o_H': [row[7] for row in so2][1::],
           'o_CH': [row[8] for row in so2][1::],
           'o_SI_CH': [row[9] for row in so2][1::],
           'o_CC': [row[10] for row in so2][1::],
           'o_SI_CC': [row[11] for row in so2][1::],
           'o_poros': [row[12] for row in so2][1::],
           'o_H20': [row[13] for row in so2][1::]}
    return res
#%% MESH CONCENTRATIONS 1D
print('\n1D data set')
n =100 #data set size
x1 = np.linspace(0.0, 0.02, n)# C
c= x1#map(np.ravel, np.meshgrid(x1))
ca = np.ones(np.shape(c))*0.01
water = np.ones(np.shape(c))
ch = np.zeros(np.shape(c))
cc = np.zeros(np.shape(c))

it=time.time() 
n_cases = len(ca)
res = phreeqc_generate_output(c, ca, ch, cc, water, n_cases)
print('Total time %s'%str(time.time()-it)) #0.0539999008179 s on my PC
df = pd.DataFrame(data=res)
# Save
df.to_pickle(root_dir+'\\data\\01_ch1D.pkl')  

#%% Time for large n 
#'''
print('\n1D large data set')
n =10000
x1 = np.linspace(0.0, 0.02, n)# C
c= x1#map(np.ravel, np.meshgrid(x1))
ca = np.ones(np.shape(c))*0.01
water = np.ones(np.shape(c))
ch = np.zeros(np.shape(c))
cc = np.zeros(np.shape(c))

n_cases = len(ca)
it=time.time() 
res = phreeqc_generate_output(c, ca, ch, cc, water, n_cases)
print('Total time %s'%str(time.time()-it))  #3.29399991035 s on my PC
#'''
#%% MESH CONCENTRATIONS 2D

print('\n2D data set')
n =100 #n x n data set
x1 = np.linspace(0.0, 0.02, n)
x2 = np.linspace(0.0, 0.02, n)
c,ca = map(np.ravel, np.meshgrid(x1,x2))

water = np.ones(np.shape(c))
ch = np.zeros(np.shape(c))
cc = np.zeros(np.shape(c))

it=time.time() 
n_cases = len(ca)
res = phreeqc_generate_output(c, ca, ch, cc, water, n_cases)
print('Phreeqc time %s'%str(time.time()-it)) #13.2969999313 s on my PC
df = pd.DataFrame(data=res)

df.to_pickle(root_dir+'\\data\\02_ch2D.pkl')  

#%% MESH CONCENTRATIONS 3D
'''
n =10
x1 = np.linspace(0.0, 0.02, n)
x2 = np.linspace(0.0, 0.02, n)
x5 = np.linspace(0.5, 1, n) #water=porosity
c,ca,water = map(np.ravel, np.meshgrid(x1,x2,x5))
ch = np.zeros(np.shape(c))
cc = np.zeros(np.shape(c))
'''
#%%
#TODO dependency between N cells and computational time