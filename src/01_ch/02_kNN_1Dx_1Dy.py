# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn import neighbors
import sys, os
root_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(root_dir)

#%% LOAD 
df = pd.read_pickle(root_dir+'\\data\\01_ch1D.pkl')
pr = 'o_CC' #output calcite
feature_cols = ['i_C']
X = df.loc[:,feature_cols]
y = df[pr]

#%% KNN
# scale input to range [0,1]
scaler = MinMaxScaler(feature_range=(0, 1))

t0 = time.time()
X_s = pd.DataFrame(scaler.fit_transform(X))
print("Scaling in %.3f s" %(time.time() - t0))

# create model
K = 4 #hyperparameter
model = neighbors.KNeighborsRegressor(n_neighbors = K, weights='distance')

# fit model
t0 = time.time()
model.fit(X_s, y)  #fit the model
print("Fitting in %.3f s" %(time.time() - t0))
#%% Generate output with 10000 points
n2 = 10000
temp = {'i_C': np.linspace(0.00, 0.02, n2)}
t0 = time.time()
Xnew= pd.DataFrame(scaler.fit_transform(pd.DataFrame(data=temp)))
Ynew = model.predict(Xnew)
print("Prediction in %.3f s" %(time.time() - t0))

plt.plot(Xnew.loc[:,0], Ynew, c='g', label='kNN' )
#plt.plot(X_s.loc[:,0], y, '.', label = "input")
plt.xlabel('data')
plt.ylabel('target')
plt.legend()
plt.show()

#%% Train at splitted data
from sklearn.model_selection import train_test_split
xTrain, xTest, yTrain, yTest = train_test_split(X_s, y, test_size = 0.1, random_state = 1)

#splitted model
K = 4 #hyperparameter
model_s = neighbors.KNeighborsRegressor(n_neighbors = K, weights='distance')
model_s.fit(xTrain, yTrain)  #fit the model
pred=model_s.predict(xTest) #make prediction on test set
print('Score is %s' %str(model_s.score(xTest, yTest)))

plt.plot(xTest.loc[:,0], pred, 'go', label='predict' )
plt.plot(xTest.loc[:,0], yTest, 'bx', label='test' )
plt.plot(xTrain.loc[:,0], yTrain, 'r.', label = "train")
plt.xlabel('data')
plt.ylabel('target')
plt.legend()
plt.show()