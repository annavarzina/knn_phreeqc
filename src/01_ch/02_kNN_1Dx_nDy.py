# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn import neighbors
import sys, os
root_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(root_dir)

#%% LOAD 
df = pd.read_pickle(root_dir+'\\data\\01_ch1D.pkl')
feature_cols_x = ['i_C'] #input carbon
feature_cols_y = ['o_pH','i_pH']
#['o_pH','o_Alk','o_C','o_Ca','o_O','o_H', 'o_CH', 'o_CC', 'i_pH','i_Alk','i_O','i_H', 'o_poros', 'o_H20','i_poros','i_H20']
X = df.loc[:,feature_cols_x]
y = df.loc[:,feature_cols_y]

#%% KNN
# scale input to range [0,1]
scaler = MinMaxScaler(feature_range=(0, 1))

t0 = time.time()
X_s = pd.DataFrame(scaler.fit_transform(X))
print("Scaling in %.3f s" %(time.time() - t0))

# create model
K = 4
model = neighbors.KNeighborsRegressor(n_neighbors = K, weights='distance')

# fit model
t0 = time.time()
model.fit(X_s, y)  #fit the model
print("Fitting in %.3f s" %(time.time() - t0))

#%% Train at splitted data

from sklearn.model_selection import train_test_split
xTrain, xTest, yTrain, yTest = train_test_split(X_s, y, test_size = 0.1, random_state = 1)

#splitted model
K = 4 #hyperparameter
model_s = neighbors.KNeighborsRegressor(n_neighbors = K, weights='distance')
model_s.fit(xTrain, yTrain)  #fit the model
pred=model_s.predict(xTest) #make prediction on test set
print('Score is %s' %str(model_s.score(xTest, yTest)))

plt.plot(xTest.loc[:,0], pred, 'go', label='predict' )
plt.plot(xTest.loc[:,0], yTest, 'bx', label='test' )
plt.plot(xTrain.loc[:,0], yTrain, 'r.', label = "train")
plt.xlabel('data')
plt.ylabel('target')
plt.legend()
plt.show()