# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn import neighbors
import sys, os
root_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(root_dir)
#%% LOAD

df = pd.read_pickle(root_dir+'\\data\\02_ch2D.pkl')
feature_cols_y = ['o_pH','i_pH']
feature_cols_x = ['i_C', 'i_Ca']
X = df.loc[:,feature_cols_x]
y = df.loc[:,feature_cols_y]

scaler = MinMaxScaler(feature_range=(0, 1))
X_s = pd.DataFrame(scaler.fit_transform(X))

xTrain, xTest, yTrain, yTest = train_test_split(X_s, y, test_size = 0.3, random_state = 0)
#%% KNN
K = 5
model = neighbors.KNeighborsRegressor(n_neighbors = K, weights='distance')


t0 = time.time()
model.fit(xTrain, yTrain)  #fit the model
print("Fitting in %.3f s" %(time.time() - t0))

t0 = time.time()
pred=model.predict(xTest) #make prediction on test set
print("Prediction in %.3f s" %(time.time() - t0))
print(model.score(xTest, yTest))

a = 10
plt.scatter(xTest.loc[:,0][1:a], yTest.loc[:,'o_pH'][1:a], c='r', label='Data')
plt.scatter(xTest.loc[:,0][1:a], pred[:,0][1:a], c='g',marker='x',label='kNN' )
plt.xlabel('data')
plt.ylabel('target')
plt.title('kNN')
plt.legend()
plt.show()
#%% Prediction time of large data set
n2 = 10000
temp = {'i_C': np.linspace(0.00, 0.02, n2),
        'i_Ca': np.linspace(0.00, 0.02, n2)}
t0 = time.time()
Xnew= pd.DataFrame(scaler.fit_transform(pd.DataFrame(data=temp)))
Ynew = model.predict(Xnew)
print("Prediction in %.3f s" %(time.time() - t0))

